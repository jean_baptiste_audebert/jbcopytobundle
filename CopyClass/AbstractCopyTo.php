<?php

namespace Jb\CopyToBundle\CopyClass;

abstract class AbstractCopyTo {

    abstract public function copy($from, $to, $context = array());

    protected function createDirectories($pathFile){
    	$fileDir = dirname($pathFile);
    	if(!is_dir($fileDir)) {
    		mkdir($fileDir, 0775, true);
    	}
    }

}
